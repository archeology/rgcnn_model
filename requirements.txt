numpy==1.24.4
open3d==0.15.2
scikit_learn==1.3.2
torch==1.11.0
torch_geometric==2.4.0
tqdm==4.64.1
plotly==5.18.0
nbformat==5.9.2