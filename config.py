import pathlib
from dataclasses import dataclass
from torch_geometric.transforms import (
    Compose,
    RandomRotate,
    FixedPoints,
    NormalizeScale,
)


dataset_path = pathlib.Path(__file__).parent.resolve() / 'dataset'
log_dir = pathlib.Path(__file__).parent.resolve() / 'logdir'
save_dir = pathlib.Path(__file__).parent.resolve() / 'trained'
# init_weight = pathlib.Path(__file__).parent.resolve() / 'rgcnn_pytorch/init_weights/1024p_model_v2_15.pt'
init_weight = None

batch_size = 10
num_epochs = 200
learning_rate = 1e-5

num_points = 1024
transforms = Compose([
    FixedPoints(num_points, replace=False, allow_duplicates=True),
])
model_config = dict(
    vertice=num_points, 
    F = [128, 512, 1024],  # Outputs size of convolutional filter.
    K = [6, 5, 3],         # Polynomial orders.
    M = [512, 128, 2],  
    input_dim = 6,
    dropout = 0.15,
    one_layer=False,
    reg_prior=True,
    recompute_L=False,
    b2relu=True
)

train_path = pathlib.Path("/mnt/disk4/datasets/arheology/raw_taheometry_dataset/train")
test_path = pathlib.Path("/mnt/disk4/datasets/arheology/raw_taheometry_dataset/test")
